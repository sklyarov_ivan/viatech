-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 06 2013 г., 16:35
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `viatech`
--

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `date_order` int(11) NOT NULL,
  `date_pay` int(11) NOT NULL,
  `status` varchar(3) NOT NULL,
  `currency` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `price`, `firstname`, `lastname`, `email`, `date_order`, `date_pay`, `status`, `currency`) VALUES
(1, 4.6, 'ivan', 'sklyarov', 'iv@mail.cmo', 0, 0, 'OK', 'EUR'),
(2, 54.7, 'igor', 'petrov', 'petr@mail.com', 0, 0, 'NO', 'EUR'),
(3, 562.9, 'stanislav', 'gunko', 'gunko@mail.com', 0, 0, 'NO', 'EUR'),
(4, 4.6, 'sdfasf', 'dfgsdf', 'asda@moa.comm', 0, 0, 'NO', 'RUR'),
(5, 123.1, 'asdasd', 'asddsa', 'easd@mail.com', 0, 0, 'YES', 'USD');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
