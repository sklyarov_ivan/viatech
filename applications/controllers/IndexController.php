<?php
namespace applications\controllers;

class IndexController extends CController implements IController
{
    public function __construct() {
        
    }
    public function index()
    {
        if (isset($_POST['selector']))
            $selector = $this->sort ($selector);
          
        $view = new \library\templater\Template();
        $orders = new \applications\models\Order();
     
        $cowCount = $orders->count();
  
        $pagination = new \library\pagination\Paginate();  
        $pagination->set_pages($cowCount);  
  
        if (isset($_POST['page']) && !empty($_POST['page']))   
            $pagination->current_page_number = $_POST['page'];  
        $records_per_page = $pagination->rows_per_page = 3;
        $offset = $pagination->set_query_offset_append();  
     
        $result = $orders->getAll($records_per_page, $offset, $selector);

        $params = array('class'=>'pagination', 'class_current'=>'pagination-current', 'function'=>'js_paginate');  
        $pagination->build_pagination_links($params);  
        
        
        $view->set('page_title',$this->page_title);
        $view->set('pagination',$pagination->links);
        $view->set('orders', $result);
        if (!IS_AJAX)
        {
            $view->set('content',$view->render('orders.php'));
            echo $view->render('layout.php');
        }
        else 
        {
            echo $view->render('orders.php');
        }

       
    }
    
    public function sort($selector)
    {
        if (IS_AJAX)
        {
            $selector = $_POST['selector'];
            switch (trim($selector))
            {
            case 'price':
                $selector = 'price';
                break;
            case 'currency':
                $selector = 'currency';
                break;
            case 'firstname':
                $selector = 'firstname';
                break;
            case 'lastname':
                $selector = 'lastname';
                break;
            case 'email':
                $selector = 'email';
                break;
            case 'order date':
                $selector = 'date_order';
                break;
            case 'pay date':
                $selector = 'date_pay';
                break;
            case 'status':
                $selector = 'status';
                break;
            default:
                $selector = 'id';
            }
            
            return $selector;
        }
        else
        {
            return;
        }
    }
}
