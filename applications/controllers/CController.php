<?php
namespace applications\controllers;

abstract class CController
{
    public $page_title;
    public $container;
    public function __construct() {
        $this->page_title = 'TEST PAGE';
        $this->container = array();
    }
}