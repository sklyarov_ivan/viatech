<?php
namespace applications\models;

interface IModel
{
    function getAll($limit, $offset);
    function setItem(array $data);
    function getItem($id);
    function updateItem($id, array $data);
    function deleteItem($id);
}