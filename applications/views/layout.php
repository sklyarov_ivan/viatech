<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $page_title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="additions/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <h1>ORDERS</h1> 
    <div class="span10">
        <div id="pagination">
            <?php echo $content?>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="additions/js/bootstrap.min.js"></script>
    <script src="additions/js/pagination.js"></script>
    <script src="additions/js/sort.js"></script>
    
  </body>
</html>