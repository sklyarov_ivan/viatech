
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th width = "50">id</td>
                    <th width = "70">price</th>
                    <th width = "30">currency</th>
                    <th width = "150">firstname</td>
                    <th width = "150">lastname</td>
                    <th width = "150">email</th>
                    <th width = "100">order date</th>
                    <th width = "100">pay date</th>
                    <th width = "50">status</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $order):?>
            <tr>
                <td><?php echo $order->id?></td>
                <td><?php echo $order->price?></td>
                <td><?php echo $order->currency?></td>
                <td><?php echo $order->firstname?></td>
                <td><?php echo $order->lastname?></td>
                <td><?php echo $order->email?></td>
                <td><?php echo $order->date_order?></td>
                <td><?php echo $order->date_pay?></td>
                <td><?php echo $order->status?></td>
                
            </tr>
            <?php endforeach?>
            </tbody>
        </table>
        <?php echo $pagination?>