<?php  

  namespace library\pagination;
  
 class Paginate {  
  

    /** 
     * How many records do we want per page 
     * @var integer 
     */  
    public $rows_per_page = 3 ;  
  
    /** 
     * This variable holds the number of pages we will have 
     * @var integer 
     */  
    public $pages = 0;  
  
    /** 
     * Holds the current page number. Default is page 1. 
     * @var integer 
     */  
    public $current_page_number = 1;  
  
    /** 
     * How many records to display in SQL. 
     * @var integer 
     */  
    public $sql_offset = 0;  
  
    /** 
     * This will be appended to the SQL query that is run to retrieve records 
     * @var string 
     */  
    public $sql_append = '';  
  
    /** 
     * Holds total records received from query 
     * @var integer 
     */  
    public $query_total_records = 0;  
  
    /** 
     * Holds the output for the generated pagination links 
     * @var string 
     */  
    public $links = '';  
  
    /** 
     * How many links to display before and after the current page 
     * @var integer 
     */  
    public $link_spread = 4;  
  
    public function __construct() 
    {
    }
    /** 
     * Given the total records returned by a query we can set up how many 
     * pages are needed. 
     * @param   int $query_total_records is the total record count of the  
     *          returned query 
     */ 
    public function set_pages($query_total_records) {  
        $this->query_total_records = $query_total_records;  
        $this->pages = ceil($this->query_total_records / $this->rows_per_page);  
    }  
  
    /** 
     * This method will set sql_append with the correct offset to append 
     * to our queries so that page navigation works. 
     */  
    public function set_query_offset_append() {  
        if ($this->current_page_number > $this->pages)   
            $this->current_page_number = $this->pages;  
        if ($this->current_page_number < 1)  
            $this->current_page_number = 1;  
        // calculate offset bases on rows per page  
        $this->sql_offset = ($this->current_page_number - 1) * $this->rows_per_page;  
        // if we have more records than rows per page append offset  
        if ($this->query_total_records > $this->rows_per_page)   
            return $this->sql_offset;
    }  
  
    /** 
     * This method will build pagination links and set the  
     * pagination_links with the proper HTML 
     * @param   array $params used to give extra options 
     *          class = span class 
     *          class_current = span class of the current page number 
     *          function = name of the function that will do the AJAX magic whatever(pageNum) 
     */  
    public function build_pagination_links($params = array()) {  
        if ($this->current_page_number > 1) {  
            $this->links = '<span ';  
            $this->links .= (!empty($params['class']))      ? 'class="'.$params['class'].'"' : '';  
            $this->links .= '><a href="#" ';  
            $this->links .= (!empty($params['function']))   ? 'onclick="'.$params['function'].'(1);"' : '';  
            $this->links .= '><< First</a></span> <span ';  
            $this->links .= (!empty($params['class']))      ? 'class="'.$params['class'].'"' : '';  
            $this->links .= '><a href="#" ';  
            $prev_page = $this->current_page_number - 1;  
            $this->links .= (!empty($params['function']))   ? 'onclick="'.$params['function'].'(' . $prev_page . ');"' : '';  
            $this->links .= '>< Prev</a></span> ';  
        }  
  
        // loop over the rest of the links  
        for ($i=($this->current_page_number-$this->link_spread);$i<(($this->current_page_number+$this->link_spread)+1);$i++) {  
            if (($i > 0) && ($i <= $this->pages)) {  
                if ($i == $this->current_page_number) {  
                    // current page link  
                    $this->links .= '<span ';  
                    $this->links .= (!empty($params['class_current'])) ? 'class="'.$params['class_current'].'"' : '';   
                    $this->links .= '><b>'.$i.'</b></span> ';  
                } else {  
                    // not current link  
                    $this->links .= '<span ';  
                    $this->links .= (!empty($params['class']))      ? 'class="'.$params['class'].'"' : '';  
                    $this->links .= '><a href="#" ';  
                    $this->links .= (!empty($params['function']))   ? 'onclick="'.$params['function'].'(' . $i . ');"' : '';  
                    $this->links .= '>' . $i . '</a></span> ';  
                }  
  
            }  
        }  
  
        if ($this->current_page_number != $this->pages) {  
            // not on last page           
            $this->links .= '<span ';  
            $this->links .= (!empty($params['class']))      ? 'class="'.$params['class'].'"' : '';  
            $this->links .= '><a href="#" ';  
            $next_page = $this->current_page_number + 1;  
            $this->links .= (!empty($params['function']))   ? 'onclick="'.$params['function'].'(' . $next_page . ');"' : '';  
            $this->links .= '>Next ></a></span> ';  
            $this->links .= '<span ';  
            $this->links .= (!empty($params['class']))      ? 'class="'.$params['class'].'"' : '';  
            $this->links .= '><a href="#" ';  
            $this->links .= (!empty($params['function']))   ? 'onclick="'.$params['function'].'(' . $this->pages . ');"' : '';  
            $this->links .= '>Last >></a></span> ';  
        }  
    }  
  
 }  
  
 ?>  