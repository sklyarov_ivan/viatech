<?php
namespace library\controller;

interface IFrontController
{
    /**
     * set controller 
     * @param type $controller
     */
    function setController($controller);
    /**
     * set action
     * @param type $action
     */
    function setAction($action);
    /**
     * set parametrs for action
     * @param array $params
     */
    function setParams(array $params);
    /**
     * start call_user_fanc and do request to controller/action
     */
    function run();
}