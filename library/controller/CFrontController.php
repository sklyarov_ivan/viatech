<?php
namespace library\controller;
class CFrontController implements IFrontController
{
    const DEFAULT_CONTROLLER = 'IndexController';
    const DEFAULT_ACTION = 'index';
    const CONTROLLERS_FOLDER = 'applications\controllers\\';
    protected $controller = self::DEFAULT_CONTROLLER;
    protected $action = self::DEFAULT_ACTION;
    protected $params = array();
    /**
     * request to parse uri
     */
    public function __construct() 
    {
        $this->parseUri();
    }
    /**
     * get parts of uri and set it
     */
    protected function parseUri()
    {
        $path = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        $current_url = explode('/', $path);
        if (isset($current_url[1]) AND !empty($current_url[1]))
            $this->setController($current_url[1]);
        if (isset($current_url[2]) AND !empty($current_url[2]))
            $this->setAction($current_url[2]);
        if (isset($current_url[3]))
        {
            $params = array();
            for($i=3;$i<=count($current_url)-1;$i++)
            {
                $params[]=$current_url[$i];
            }
            $this->setParams($params);
        }
    }
   
    public function setController($controller) 
    {
        $controller = ucfirst(strtolower($controller)).'Controller';
        
        try
        {
            if (!class_exists(self::CONTROLLERS_FOLDER.$controller))
                throw new \Exception('404');
                $this->controller = $controller;
            return $this;
        }
        catch (\Exception $e)
        {
            echo $e->getMessage(); 
        }
    }
    public function setAction($action) 
    {
        $reflection =new \ReflectionClass(self::CONTROLLERS_FOLDER.$this->controller);
        try
        {
            if (!$reflection->getMethod($action))
                throw new \Exception('404');
        }
        catch(\Exception $e)
        {
            echo $e->getMessage();
        }
    }
    public function setParams(array $params) 
    {
      $this->params = $params;
      return $this;
    }
    public function run() 
    {
        $controller = self::CONTROLLERS_FOLDER.$this->controller;
        call_user_func(array(new $controller, $this->action), $this->params);
    }
}