<?php
namespace library\templater;
class Template {
  private $vars  = array();
 
  public function get($name) {
    return $this->vars[$name];
  }
 
  public function set($name, $value) {
    if($name == 'layout') {
      throw new Exception("Cannot bind variable named 'view_template_file'");
    }
    $this->vars[$name] = $value;
  }
 
  public function render($view_template_file) {
    if(array_key_exists('view_template_file', $this->vars)) {
      throw new Exception("Cannot bind variable called 'view_template_file'");
    }
    extract($this->vars);
    ob_start();
    include(VIEWS.$view_template_file);
    return ob_get_clean();
  }
}