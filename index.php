<?php
define('CONFIG', '\config\\');
define('VIEWS','\applications\views\\');
define('IS_AJAX',(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND 
          strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')?true:false);
function __autoload($classname)
{
    $filename = '/'.$classname.'.php';
    include_once $filename;
}
use library\controller\CFrontController;

$frontController = new CFrontController();
$frontController->run();